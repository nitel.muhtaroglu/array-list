package cs102;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> integerList = new ArrayList<Integer>();
        ArrayList<Double> doubleList = new ArrayList<Double>();
        ArrayList<String> stringList = new ArrayList<String>();

        integerList.add(42);
        doubleList.add(42.0);
        stringList.add("42");
    }
}
